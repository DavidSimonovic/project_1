<?php

namespace app\Models;

use \app\Database;

class User
{

    /**
     * pdo
     *
     * @var mixed
     */
    protected $pdo;

    /**
     * stmt
     *
     * @var mixed
     */
    protected $stmt;

    /**
     * name
     *
     * @var string
     */
    protected $name;

    /**
     * lastname
     *
     * @var string
     */
    protected $lastname;

    /**
     * email
     * 
     * @var string
     */
    private $email;
    /**
     * password
     * 
     * @var string
     */
    private $password;
    private $check;
    private $authEmail;
    private $authPass;
    private $authRole;
    private $sql;
    private $role;
    private $users;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {

        $conn = new \app\Database\DB;

        $this->pdo = $conn->connect();
    }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {

        $sql = "SELECT * FROM users";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();

        $users = $stmt->fetchAll();

        return $users;
    }
    /**
     * show
     * 
     * @param int
     * 
     * @return array
     */
    public function show($id)
    {

        $stmt = $this->pdo->prepare("SELECT * FROM users WHERE id = ?");
        $stmt->execute([$id]);

        $user = $stmt->fetch();

        return $user;
    }

    /**
     * update
     * 
     * @param string
     * @param string
     * @param int
     * 
     * @return void
     */

    public function update($name, $lastname, $id)
    {

        $sql = "UPDATE users SET name=?, lastname=? WHERE id=?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([$name, $lastname, $id]);
    }

    /**
     * store
     *
     * @param  mixed $data
     * @return void
     */
    public function store($data)
    {
        if (!isset($data['role'])) {
            $data['role'] = 1;
        }
        unset($data['repassword']);

        $stmt = $this->pdo->prepare("INSERT INTO users (name,lastname,email,password,role) VALUES (?,?,?,?,?)");

        $stmt->execute([$data['name'], $data['lastname'], $data['email'], $data['password'], $data['role']]);
    }

    /**
     * userExists
     *
     * @param  string $email
     * @return void
     */
    public function userExists($email)
    {

        $stmt = $this->pdo->prepare("SELECT email FROM users WHERE email = ?");
        $stmt->execute([$email]);

        $check = $stmt->fetch();

        return $check;
    }


    /**
     * authUser
     * 
     * @param  string $email
     * @param  string $pass
     * @return void
     */
    public function authUser($credentials)
    {

        $stmt = $this->pdo->prepare("SELECT * FROM users WHERE email = ?");
        $stmt->execute([$credentials['email']]);

        $dbUser = $stmt->fetch();

        $authEmail = $dbUser['email'];
        $authPass = $dbUser['password'];


        if ($credentials['email'] == $authEmail && $authPass == crypt($credentials['password'], 'test')) {

            return true;
        } else {

            return false;
        }
    }

    /**
     * userRole
     *
     * @param  string $id
     * @return int
     */
    public static function userRole($email)
    {

        $stmt = (new self)->pdo->prepare("SELECT * FROM users WHERE email = ?");
        $stmt->execute([$email]);

        $dbUser = $stmt->fetch();

        if ($dbUser['role'] == $_ENV['DB_ADMIN']) {

            return true;
        } else {

            return false;
        }
    }


    /**
     * userIdByEmail
     * 
     * @param string
     * @return int
     */
    public static function userIdByEmail($email)
    {

        $stmt = (new self)->pdo->prepare("SELECT id FROM users WHERE email = ?");
        $stmt->execute([$email]);

        $id = $stmt->fetch();

        return (int) $id['id'];
    }

    /**
     * delete
     * 
     * @param int
     */
    public function delete($id)
    {

        $stmt = $this->pdo->prepare("DELETE FROM users WHERE id = ?");
        $stmt->execute([$id]);
    }
}
