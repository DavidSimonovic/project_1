
CREATE DATABASE IF NOT EXISTS project_1;

USE project_1;

DROP TABLE IF EXISTS users;

CREATE TABLE users(
	name varchar(40) not null,
    lastname varchar(30) not null,
	email varchar(40) not null unique,
	password varchar(80) not null,
    id int not null auto_increment,
    role int not null default 1,
	primary key(id)
);
