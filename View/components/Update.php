<main class="my-form">
    <div class="cotainer">
        <div class="row justify-content-center mt-5">
            <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Update info</div>
                        <div class="card-body">
                            <form name="my-form" action="/profile/update/<?php echo $user['id']; ?>" method="POST">
                              
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                                    <div class="col-md-6">
                                       <input type="text" id="name" class="form-control" name="name" placeholder="<?php echo $user['name']; ?>">
                                    </div>
                                </div>
                                </br> 
                                <div class="form-group row">
                                    <label for="lastname" class="col-md-4 col-form-label text-md-right">Lastname</label>
                                    <div class="col-md-6">
                                        <input type="text" id="lastname" class="form-control" name="lastname" placeholder="<?php echo $user['lastname']; ?>">
                                    </div>
                                </div>
                                </br>
                             
                                <div class="form-group row">
                                <div class="col-md-4 col-sm-12 mx-auto">
                                        <button type="submit" class="btn btn-primary">
                                                Update
                                        </button>
                                        </div>
                                    </div>
                               
                                <?php 
                              
                                if(isset($_GET['message'])){
                                  ?>
                                         <div class="form-group row">
                                        <div class="col-md-4 col-sm-12 mx-auto mt-5">
                                        <div class="alert alert-success" role="alert">
                                        <h3 class="text-center">
                                  <?php 
                                              if($_GET['message'] == 'successupdate'){
                                                echo  'Your info was updated';
                                              }
                                             ?>
                                </h3>
                                </div>
                                <?php
                                              }
                                              ?>
                               
                                              </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <!-- Tabs Titles -->
    
</main>
