<main class="my-form">
    <div class="cotainer">
        <div class="row justify-content-center mt-5">
            <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Register</div>
                        <div class="card-body">
                            <form name="my-form" action="/registration" method="POST">
                              
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                                    <div class="col-md-6">
                                       <input type="text" id="name" class="form-control" name="name">
                                    </div>
                                </div>
                                </br> 
                                <div class="form-group row">
                                    <label for="lastname" class="col-md-4 col-form-label text-md-right">Lastname</label>
                                    <div class="col-md-6">
                                        <input type="text" id="lastname" class="form-control" name="lastname">
                                    </div>
                                </div>
                                </br> 
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                    <div class="col-md-6">
                                        <input type="text" id="email" class="form-control" name="email">
                                    </div>
                                </div>
                                </br> 
                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                    <div class="col-md-6">
                                        <input type="password" id="password" class="form-control" name="password">
                                    </div>
                                </div>
                                    </br> 
                                <div class="form-group row">
                                    <label for="repassword" class="col-md-4 col-form-label text-md-right">Repeat Password</label>
                                    <div class="col-md-6">
                                        <input type="password" id="repassword" class="form-control" name="repassword">
                                    </div>
                                </div>
                                </br>
                                <div class="form-group row">
                                <div class="col-md-4 col-sm-12 mx-auto">
                                        <button type="submit" class="btn btn-primary">
                                        Register
                                        </button>
                                        </div>
                                    </div>



                               
                                <?php 
                              
                                if(isset($_GET['message'])){
                                  ?>
                                         <div class="form-group row">
                                <div class="col-md-4 col-sm-12 mx-auto mt-5">
                                <div class="alert alert-danger" role="alert">
                                <h3 class="text-center">
                                  <?php 
                                              if($_GET['message'] == 'passnotmatch'){
                                                echo  'Your password does not match';
                                              }
                                              if($_GET['message'] == 'userexists'){
                                                echo  'User exists';
                                              }
                                              if($_GET['message'] == 'missingname'){
                                                echo  'Enter your name';
                                              }
                                              if($_GET['message'] == 'missinglastname'){
                                                echo  'Enter your last name';
                                              }

                                              if($_GET['message'] == 'notvalidemail'){
                                                echo  'You email is not valid';
                                              }
                                              if($_GET['message'] == 'emptypass'){
                                                echo  'You did not enter any password';
                                              }
                                              ?>
                                               </h3>
                                              <?php
                                              }
                                              ?>
                               
                                              </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <!-- Tabs Titles -->
    
</main>
