<?php 
if (isset($_SESSION['logedin']) && $_SESSION['logedin']) {
?>
 
<table class="mx-auto">
  <tr>
    <th>Name</th>
    <th>Lastname</th>
    <th>Email</th>  
  </tr>

<?php 

  foreach($users as $user){

?>
      
  <form method="POST" action="/delete/<?php echo $user['id']; ?>">
  <tr>
    <td><?php echo $user['name']; ?></td>
    <td><?php echo $user['lastname']; ?></td>
    <td><?php echo $user['email']; ?></td>
    <td>
    <?php 
    if ($_SESSION['role']){
      
      ?>
      <button type="submit" class="btn btn-danger">Delete</button>
      
    <?php
    
    ?>
    </td>
    </form>

    <form action="/profile/<?php echo $user['id']; ?>">
    <td> 
      <button type="submit" class="btn btn-info">Edit</button>
    </td>    
    </tr>
  </form>

  
  <?php
    }
  }


?>

</table>

<?php

} else {

    include 'Registration.php';
}

 ?>