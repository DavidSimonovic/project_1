<?php 
include __DIR__ . '/../vendor/autoload.php';

use \ByJG\Util;
use \ByJG\DbMigration;
use \app\Models\User;


$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__.'/../');
$dotenv->load();

/**
 * 
 * Database migration connection 
 * 
 */
$connectionUri = new \ByJG\Util\Uri('mysql://'.$_ENV['DB_USER'].':'.$_ENV['DB_PASS'].'@'.$_ENV['DB_SERVER'].':3306/'.$_ENV['DB_NAME']);

$migration = new \ByJG\DbMigration\Migration($connectionUri, '../Migrations/');

$migration->registerDatabase('mysql', \ByJG\DbMigration\Database\MySqlDatabase::class);

$migration->addCallbackProgress(function ($action, $currentVersion, $fileInfo) {
    echo "$action, $currentVersion, ${fileInfo['description']}\n";
});


$migration->reset();

$migration->update($version = null);




$faker = Faker\Factory::create();

/**
 * Faker for user creation
 * 
 */
for ($i = 0; $i < 40; $i++) {

    $data = [];

    $data['name'] = $faker->firstname;
    $data['lastname'] = $faker->lastname;
    $data['email'] = $faker->unique()->email;
    $data['role'] = 1;
    $data['password'] = crypt($faker->password,'test');

    $user = (new User())->store($data);

}
/**
 * Faker for admin creation 
 * 
 */ 
    $data['name'] = 'admin';
    $data['lastname'] = 'admin';
    $data['email'] = 'admin@admin.com';
    $data['role'] = 2;
    $data['password'] = crypt('admin','test');

    $user = (new User())->store($data);
