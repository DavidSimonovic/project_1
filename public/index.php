<?php

use app\Controllers\HomeController;

use app\Controllers\RegistrationController;

use app\Controllers\LoginController;

use app\Controllers\UserController;

use app\Controllers\ProfileController;

require __DIR__ . '/../vendor/autoload.php';


     $router = new \Bramus\Router\Router();

     $router->get('/','app\Controllers\HomeController@index');
     $router->get('/registration', 'app\Controllers\RegistrationController@index');
     $router->get('/login','app\Controllers\LoginController@index');
     $router->post('/login','app\Controllers\LoginController@login');
     $router->get('/logout','app\Controllers\LoginController@logout');
     $router->post('/registration', 'app\Controllers\RegistrationController@addUser');  
     $router->post('/delete/{id}','app\Controllers\UserController@delete');
     $router->get('/profile','app\Controllers\ProfileController@index');
     $router->post('/profile/update/{id}','app\Controllers\ProfileController@update');
     $router->get('/profile/{id}','app\Controllers\ProfileController@index');

     $router->run();