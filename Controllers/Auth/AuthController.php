<?php

namespace app\Controllers\Auth;

use app\Models\User;

class AuthController
{
    /**
     * login
     * 
     * @param array
     * @return bool
     */
    public static function login($credentials)
    {

        return (new self)->loginCheck($credentials);
    }

    /**
     * loginCheck
     * 
     * @param array
     * @return bool
     */
    protected function loginCheck($credentials)
    {

        return $loginCheck = (new User())->authUser($credentials);
    }

    /**
     * isAdmin
     * 
     * @param array
     * @return bool
     */
    public static function isAdmin($email)
    {
        return (new self)->adminCheck($email);
    }

    /**
     * adminCheck
     * 
     * @param array
     * @return bool
     */
    protected function adminCheck($email)
    {

      return User::userRole($email);
    }

       
}

