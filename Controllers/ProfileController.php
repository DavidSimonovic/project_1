<?php

namespace app\Controllers;

use app\Models\User;
use app\Controllers\Controller;

class ProfileController extends Controller
{


    public function index()
    {   

            $user = (new User())->show($_SESSION['user_id']);
        
            include '../View/Profile.php';
    }

    public function update($id)
    {
        
        $name = $_POST['name'];
        $lastname = $_POST['lastname'];

        if (!empty($name && !empty($lastname))){  
         $update = (new User())->update($name, $lastname, $id);

         header('Location: ../../profile?message=successupdate');
    }
}
}
