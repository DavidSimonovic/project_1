<?php

namespace app\Controllers;

use app\Controllers\Auth\AuthController;
use \app\Models\User;

class LoginController extends Controller
{
  /**
   * user
   *
   * @var string
   */
  public $user;

  /**
   * email
   *
   * @var string
   */
  private $email;

  /**
   * password
   *
   * @var string
   */
  private $password;

  /**
   * login
   *
   * @return void
   */
  public function login()
  {

    $credentials = [];

    foreach ($_POST as $key => $value) {
        $credentials[$key] = $value;
    }

    if (AuthController::login($credentials)) {

      session_start();

     
      $_SESSION['logedin'] = true;
      $_SESSION['role'] = AuthController::isAdmin($credentials['email']);
      $_SESSION['user_id'] = User::userIdByEmail($credentials['email']);
  
      header('Location: /?message=logedin');
    } else {

      header('Location: login?message=wrongcred');
    }
  }


  /**
   * logout
   *
   * @return void
   */
  public function logout()
  {
    session_start();
    unset($_SESSION["logedin"]);
    unset($_SESSION["role"]);
    unset($_SESSION['user_id']);

    session_destroy();
    header('Location: /');
  }
}
