<?php
namespace app\Controllers;

use \app\Models\User;

class HomeController extends Controller
{

    /**
     * index
     *
     * @return void
     */
    public function index()
    {

        $users = (new User())->index();
        
        include '../View/Home.php';
    }

}