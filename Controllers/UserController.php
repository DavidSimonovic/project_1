<?php

namespace app\Controllers;

use \app\Models\User;

class UserController extends Controller
{
   /**
     * delete
     * 
     * @param int
     * @return void
     * 
     */
    public function delete($id)
    {
        $user = (new User())->delete($id);
        header('Location: /');
    }
}
