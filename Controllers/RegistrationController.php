<?php

namespace app\Controllers;

use \app\Models\User;
use app\Validators\RegistrationValidation;

class RegistrationController extends Controller
{
    
  
    /**
     * data
     *
     * @var mixed
     */
    private $data;     

    /**
     * user
     *
     * @var mixed
     */
    protected $user;
    
    /**
     * addUser
     *
     * @return void
     */
    public function addUser()
    {
        $user = new User();

        $data = [];

        foreach ($_POST as $key => $value) {
            $data[$key] = $value;
        }

        if (RegistrationValidation::passed($data)) {

             $user->store($data);

             header('Location: login?message=success');

             }
                        
        }
                    
    }
                