<?php

namespace app\Validators;

use app\Models\User;

class RegistrationValidation {

    /**
     * passed
     * 
     * @param array
     * @return void
     */
    public static function passed($data): bool
    {
        return (new self)->validate($data);
    }

    /**
     * validate
     * 
     * @param array
     * @return bool
     */
    protected function validate($data): bool
    {
        $user = new User();

        if (empty($data['name'])) {

            header('Location: registration?message=missingname');
        
        } elseif (empty($data['lastname'])) {

            header('Location: registration?message=missinglastname');
        
        } elseif (empty($data['password'])) {

            header('Location: registration?message=emptypass');
        
        } elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {

             header('Location: registration?message=notvalidemail');
        
        } elseif ($user->userExists($data['email']) != 0) {

             header('Location: registration?message=userexists');
        
        } elseif ($data['password'] != $data['repassword']) {

             header('Location: registration?message=passnotmatch');
        }  else {

            return true;
        } 
    }

}